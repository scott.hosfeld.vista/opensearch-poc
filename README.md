#OpenSearch local POC

# General setup

These are general instructions for getting an OpenSearch cluster running locally using Docker Compose. Refer to the [OpenSearch download page](https://opensearch.org/downloads.html) for more info. And [general docs](https://opensearch.org/docs/latest/). 

## run the cluster
- get the [docker-compose yml file](./docker-compose.yml)
- add expose "9200" to allow API access from outside the container

## create an index
- UI: Index Management / Indices / Create Index
- or do it through the API

## Get Data
- This was done using the [Algolia CLI to dump index data](https://www.algolia.com/doc/tools/cli/commands/algolia-objects/#algolia-objects-browse) in the form of NDJSON (its native format from the CLI). 

## index the data
- get data as ndjson format
- add the "action" to be done for each object on a line before the object
  - `{"create": {}}`
- call POST https://0.0.0.0:9200/{index-name}/_bulk with all actions+objects as payload
- example
```
{"create": {}}
{"attributeFacets":{"Colour":["Black"],"ancestors":[]},"mpv":"wirelessBusinessEarbud","objectID":"wirelessBusinessEarbud","searchFields":{"tags":["Black"]},"title":"Wireless Business Earbud"}
```

query the data
- GET https://0.0.0.0:9200/{index-name}/_search - should return all results
- /_search?size=100 - limit to top 100 

# setup ML for vector DB
```
PUT _cluster/settings
{
   "persistent":{
     "plugins.ml_commons.only_run_on_ml_node": false
   }
}
```

# Vector/semantic search
## create a new index for the data that will hold vectors
See steps [above](#create-an-index). I called mine `products-vect`


## setup model
follow steps here
https://opensearch.org/docs/latest/ml-commons-plugin/pretrained-models/


here's the response with the model_id
```
{
  "model_id": "gUieJYsBORxJGFCf8TDZ",
  "task_type": "DEPLOY_MODEL",
  "function_name": "TEXT_EMBEDDING",
  "state": "COMPLETED",
  "worker_node": [
    "8BHdnS7sS9OpFU4oolSH8A",
    "nOPoHDJ7RqWRJac96ANqRQ"
  ],
  "create_time": 1697143377721,
  "last_update_time": 1697143391387,
  "is_async": true
}
```

## create pipeline 
- this will be invoked when data is added to the index in order to create the vectors

```
PUT _ingest/pipeline/product-vector-pipe
{
  "description": "Pipeline for embedding product data",
  "processors" : [
    {
      "text_embedding": {
        "model_id": "gUieJYsBORxJGFCf8TDZ",
        "field_map": {
           "title": "title_embed",
           "searchFields.tags":"tags_embed"
        }
      }
    }
  ]
}
```

## Search using the vector
```
GET /products-vect/_search
{
  "_source": [
    "title"
  ],
  "query": {
    "bool": {
      "must": [
        {
          "neural": {
            "title_embed": {
              "query_text": "box labels?",
              "model_id": "gUieJYsBORxJGFCf8TDZ",
              "k":100
            }
          }
        }
      ]
    }
  }
}
```